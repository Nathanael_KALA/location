export const routePaths = {
  root: "/",
  auth: "/auth",
  login: "/auth/login",
  home: "/home",
  location: "/home/location",
  locationDate: "/home/location/date",
  locationCategories: "/home/location/list",
  locationCategoriesArticles: "/home/location/list/:id_cat",
  locationSubCategories: "/home/location/list/:id_cat/subCat",
  locationSubCategoriesArticles:
    "/home/location/list/:id_cat/subCat/:id_subCat",
  locationReserve: "/home/location/reserve",
  locationConfirmCommand: "/home/location/confirm-command",
  locationDetails: "/home/location/details/:id_command",
  admin: "/home/admin",
};
